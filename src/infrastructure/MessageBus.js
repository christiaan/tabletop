class MessageBus {
  constructor() {
    this._handlers = []
  }

  handle(message) {
    this._handlers.forEach(handler => handler(message));
  }

  registerHandler(handler) {
    if (typeof handler.handle === 'function') {
      handler = handler.handle.bind(handler)
    }

    if (typeof handler !== 'function') {
      throw new Error('Invalid handler given. Should either be a function or ' +
        'a Object that has a handle method.');
    }

    this._handlers.push(handler)
  }
}

module.exports = MessageBus;
