'use strict';

module.exports = {
  CreateTable: function (params) {
    return this.$aggregate.create('Table')
      .then(function (table) {
        return table.$save();
      });
  },

  PlaceGameSet: function (params) {
    return this.$aggregate.load('Table', params.id)
      .then(function (table) {
        table.placeGameSet(params.gameSet);
        return table.$save();
      });
  },

  MoveGamePiece: function (params) {
    return this.$aggregate.load('Table', params.id)
      .then(function (table) {
        table.moveGamePiece(params.pieceId, params.position);
        return table.$save();
      });
  },

  FlipGamePiece: function (params) {
    return this.$aggregate.load('Table', params.id)
      .then(function (table) {
        table.flipGamePiece(params.pieceId);
        return table.$save();
      });
  }
};
