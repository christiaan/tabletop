"use strict";
// load jquery
var $ = require('jquery');
var TableProjection = require('./src/client/TableProjection');
var DragHandler = require('./src/client/DragHandler');
var io = require('socket.io-client');
var P2P = require('socket.io-p2p');
var table = require('./src/domain/table');
var eventric = require('eventric');
eventric.log.setLogLevel('debug');
var socket = io();
var p2psocket = new P2P(socket);
var eventricRemoteSocketIoClient = require('eventric-remote-socketio-client');

// or load just the modules you need
require('jquery-ui/draggable');
require('es6-promise').polyfill();

$(function () {
  var $tabletop = $('.tabletop');
  var dragHandler = new DragHandler($tabletop, p2psocket);
  var projection;

  dragHandler.on('dropped', function (tableId, pieceId, position) {
    table.command('MoveGamePiece', {
      id: tableId,
      pieceId: pieceId,
      position: position
    });
  });

  $tabletop.on('dblclick', '.flippable', function () {
    var $piece = $(this);
    var tableId = $piece.closest('[data-table-id]').data('table-id'),
      pieceId = $piece.data('piece-id');
    table.command('FlipGamePiece', {
      id: tableId,
      pieceId
    });
  });

  p2psocket.on('upgrade', function () {
    console.log('Peer to pear!')
  });

  eventricRemoteSocketIoClient.initialize({ioClientInstance: socket})
    .then(function() {
      table = eventric.remote('table')
        .addClient('socketio', eventricRemoteSocketIoClient)
        .set('default client', 'socketio');

      if (window.location.hash && window.location.hash !== '#') {
        projection = new TableProjection($tabletop, table, window.location.hash.substr(1));
      } else {
        table.command('CreateTable').then(function (id) {
          window.location.hash = id;
          projection = new TableProjection($tabletop, table, id);

          return table.command('PlaceGameSet', {
            id,
            gameSet: require('./src/pieces/checkers')
          });
        });
      }
    });

  window.table = table;
});
