## Table ##
A unique playing field on which a set of selected **Game Pieces** are available.

## Game Piece ##
A item that can be interacted with by a **Player** which is placed on a **Table**.
Game Pieces have different properties

* Movable - The ability to move the piece around the table
* Stackable - The ability to stack another piece on it
* Flippable - The piece has two sides and can be flipped
* Random - A dice with various sides

## Player ##
Someone participating at a **Table**.
