"use strict";
module.exports = {
  TableCreated: function TableCreated(params) {
  },
  GameSetPlaced: function GameSetPlaced(params) {
    this.gameSet = params.gameSet;
  },
  GamePiecePlaced: function GamePiecePlaced(params) {
    this.piece = params.piece;
    this.pieceId = params.pieceId;
  },
  GamePieceMoved: function GamePieceMoved(params) {
    this.pieceId = params.pieceId;
    this.position = params.position;
  },
  GamePieceFlipped: function GamePieceFlipped(params) {
    this.pieceId = params.pieceId;
    this.html = params.html
  }
};
