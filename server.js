"use strict";
require('babel/register');
require('es6-promise').polyfill();

var express = require('express');
var app = express();
var http = require('http').createServer(app);
var io = require('socket.io')(http);
var p2p = require('socket.io-p2p-server').Server;
var eventric = require('eventric');
eventric.log.setLogLevel('debug');

io.use(p2p);

app.use('/dist', express.static('dist'));

app.get('/', function(req, res){
  res.sendFile(__dirname + '/index.html');
});

var remoteEndpoint = require('eventric-remote-socketio-endpoint');
remoteEndpoint.initialize({
  ioInstance: io
}, function () {
  eventric.addRemoteEndpoint('socketio', remoteEndpoint);
  var table = require('./src/domain/table');
  table.initialize().then(function () {
    http.listen(3030, function(){
      console.log('listening on *:3030');
    });
  });
});

