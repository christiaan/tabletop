"use strict";

var $ = require('jquery');

class TableProjection {
  constructor(mountNode, context, id) {
    this.mountNode = mountNode;
    this.node = $('<div class="table">').attr('data-table-id', id).appendTo(this.mountNode);
    this.pieces = [];
    this.id = id;

    context.subscribeToDomainEventWithAggregateId('GamePiecePlaced', id, domainEvent => {
      this.handleGamePiecePlaced(domainEvent);
    });

    context.subscribeToDomainEventWithAggregateId('GamePieceMoved', id, domainEvent => {
      this.handleGamePieceMoved(domainEvent);
    });

    context.subscribeToDomainEventWithAggregateId('GamePieceFlipped', id, domainEvent => {
      this.handleGamePieceFlipped(domainEvent);
    });

    context.findDomainEventsByNameAndAggregateId(['GamePiecePlaced', 'GamePieceMoved', 'GamePieceFlipped'], id).then((events)=> {
      events.forEach(event => {
        this['handle' + event.name](event);
      });
    });
  }

  handleGamePiecePlaced(domainEvent) {
    this.addPiece(
      domainEvent.payload.piece,
      domainEvent.payload.pieceId
    );
  }

  handleGamePieceMoved(domainEvent) {
    this.movePiece(domainEvent.payload.pieceId, domainEvent.payload.position);
  }

  handleGamePieceFlipped(domainEvent) {
    this.flipGamePiece(domainEvent.payload.pieceId, domainEvent.payload.html);
  }

  addPiece(piece, pieceId) {
    var node = $('<div>').html(piece.html).css({
      position: 'absolute',
      left: piece.position.left + 'px',
      top: piece.position.top + 'px'
    }).attr('data-piece-id', pieceId);

    node.toggleClass('draggable', piece.draggable);
    node.toggleClass('flippable', piece.flippable);

    this.node.append(node);
    this.pieces.push({
      position: piece.position,
      node,
      pieceId
    });
  }

  movePiece(pieceId, position) {
    this.pieces.forEach(piece => {
      if (piece.pieceId === pieceId) {
        piece.position = position
      }
      piece.node.css({
        left: piece.position.left + 'px',
        top: piece.position.top + 'px'
      });
    });
  }

  flipGamePiece(pieceId, html) {
    this.pieces.forEach(piece => {
      if (piece.pieceId === pieceId) {
        piece.node.html(html);
      }
    });
  }
}

module.exports = TableProjection;
