"use strict";
var $ = require('jquery');
var Emitter = require('emitter-component');
var throttle = require('lodash.throttle');

module.exports = DragHandler;

function DragHandler(mountNode, socket) {
  var $mountNode = $(mountNode);
  Emitter(this);

  var self = this;

  $mountNode.on('mousedown', '.draggable', function(event) {
    var $piece = $(this);
    var tableId = $piece.closest('[data-table-id]').data('table-id'),
      pieceId = $piece.data('piece-id');
    if (!$piece.is('.ui-draggable')) {
      $piece.draggable({
        drag: function (event, ui) {
          self.emit('drag',
            tableId,
            pieceId,
            ui.position
          );
        },
        stop: function (event, ui) {
          $piece.draggable("destroy");

          self.emit('dropped',
            tableId,
            pieceId,
            ui.position
          );
        }
      }).trigger(event);
    }
  });

  this.on('drag', throttle(socketEmit, 200, {leading: false}));

  socket.on('drag', function (obj) {
    var piece = $mountNode.find('[data-piece-id='+obj.pieceId+']');
    piece.css({
      left: obj.position.left + 'px',
      top: obj.position.top + 'px'
    });
  });

  function socketEmit(tableId, pieceId, position) {
    socket.emit('drag', {
      id: tableId,
      pieceId: pieceId,
      position: position
    });
  }
}
