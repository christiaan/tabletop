# Tabletop

A tabletop games simulator written as a web application.

## Development
The only requirement is to have either [nodejs][1] or [iojs][2] installed.

Start with `npm install` to install all dependencies.

Then run `npm start` and go to [localhost:3030](http://localhost:3030)

 [1]: https://nodejs.org/
 [2]: https://iojs.org/
