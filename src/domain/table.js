"use strict";
var eventric = require('eventric');

var table = eventric.context('table');

table.defineDomainEvents(require('./events'));
table.addAggregate('Table', require('./Table'));
table.addCommandHandlers(require('./commandHandlers'));

module.exports = table;
