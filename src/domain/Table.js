var uuid = require('uuid');

module.exports = Table;

function Table() {
  var pieces = {};
  var gameSet = {};

  this.create = function () {
    this.$emitDomainEvent('TableCreated');
  };
  this.handleTableCreated = function () {
  };

  this.placeGameSet = function (gameSet) {
    this.$emitDomainEvent('GameSetPlaced', {gameSet});
    gameSet.pieces.forEach(piece => {
      this.placeGamePiece(piece);
    });
  };

  this.handleGameSetPlaced = function (domainEvent) {
    gameSet = domainEvent.payload.gameSet;
  };

  this.placeGamePiece = function (piece) {
    this.$emitDomainEvent(
      'GamePiecePlaced',
      {
        piece,
        pieceId: uuid.v4()
      }
    );
  };

  this.handleGamePiecePlaced = function (domainEvent) {
    pieces[domainEvent.payload.pieceId] = domainEvent.payload.piece;
  };

  this.moveGamePiece = function (pieceId, position) {
    this.guardPieceId(pieceId);
    this.$emitDomainEvent('GamePieceMoved', {pieceId, position});
  };

  this.handleGamePieceMoved = function (domainEvent) {
    var piece = pieces[domainEvent.payload.pieceId];
    piece.position =
      domainEvent.payload.position;
  };

  this.flipGamePiece = function (pieceId) {
    this.guardPieceId(pieceId);
    var piece = pieces[pieceId];

    if (!piece.hasOwnProperty('flipside')) {
      throw new Error('GamePiece cannot be flipped; it has no flipside');
    }

    this.$emitDomainEvent('GamePieceFlipped', {
      pieceId,
      html: !piece.flipped ? piece.flipside : piece.html
    });
  };

  this.handleGamePieceFlipped = function (domainEvent) {
    var piece = pieces[domainEvent.payload.pieceId];
    piece.flipped = !piece.flipped;
  };

  this.guardPieceId = function (pieceId) {
    if (!pieces.hasOwnProperty(pieceId)) {
      throw new Error('Unknown pieceId ' + pieceId);
    }
  };
}
