var assert = require('assert');
var MessageBus = require('./MessageBus');

describe('MessageBus', ()=> {
  describe('registerHandler', ()=> {
    var mb;

    beforeEach(()=> {
      mb = new MessageBus();
    });

    it('should throw a error when given a non-function', ()=> {
      var error = null;

      try {
        mb.registerHandler(null);
      } catch (e) {
        error = e;
      }

      assert.ok(error);

    });
    it('should register a function as a handler', (done)=> {
      mb.registerHandler(()=> done() );
      mb.handle({});
    });
    it('should register a object with a handle function', (done)=> {
      mb.registerHandler({
        handle() {
          done();
        }
      });

      mb.handle({});
    });
  });
});
