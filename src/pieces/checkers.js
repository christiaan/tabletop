var zwart = {
  position: {left: 100, top: 0},
  draggable: true,
  flippable: true,
  html: `<div class="damsteen zwart"></div>`,
  flipside: `<div class="damsteen zwart gestapeld"><div class="damsteen zwart">&#9813;</div></div>`
};

var wit = {
  position: {left: 0, top: 0},
  draggable: true,
  flippable: true,
  html: `<div class="damsteen wit"></div>`,
  flipside: `<div class="damsteen wit gestapeld"><div class="damsteen wit">&#9819;</div></div>`
};

module.exports = {
  name: 'Checkers',
  pieces: [
    {
      position: {left: 50, top: 20},
      html: `<div class="board">
  <table>
  <tr>
  <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
  </tr>
  <tr>
  <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
  </tr><tr>
  <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
  </tr><tr>
  <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
  </tr><tr>
  <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
  </tr><tr>
  <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
  </tr><tr>
  <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
  </tr><tr>
  <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
  </tr>
  </table>
</div>`
    }
  ].concat(times(12, zwart)).concat(times(12, wit))
};

function times(amount, what) {
  var res = [];
  for (var i = 0; i < amount; i += 1) {
    res.push(what);
  }
  return res;
}
